void main (List<String> args) async {
  print("Kita akan bernyanyi");
  print(await line());
  print("");
  print(await line1());
  print(await line2());
  print(await line3());
  print(await line4());
  print(await line5());
  print(await line6());
  print(await line7());
  print(await line8());
  print("");
  print(await line9());
}

Future<String> line() async {
  String greeting = "Lagu pagiku cerahku matahari bersinar";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}

Future<String> line1() async {
  String greeting = "pagiku cerah matahari bersinar, Kugendong tas merahku di pundak";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}

Future<String> line2() async {
  String greeting = "S'lamat pagi semua Kunantikan dirimu Di depan kelasmu Menantikan kami";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}

Future<String> line3() async {
  String greeting = "Guruku tersayang Guru tercinta Tanpamu, apa jadinya aku";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}

Future<String> line4() async {
  String greeting = "Tak bisa baca tulis Mengerti banyak hal Guruku, terima kasihku";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}

Future<String> line5() async {
  String greeting = "Nakalnya diriku Kadang buatmu marah Namun segala maaf Kau berikan";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}

Future<String> line6() async {
  String greeting = "S'lamat pagi semua Kunantikan dirimu Di depan kelasmu Menantikan kami";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}

Future<String> line7() async {
  String greeting = "Guruku tersayang Guru tercinta Tanpamu, apa jadinya aku";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}

Future<String> line8() async {
  String greeting = "Tak bisa baca tulis Mengerti banyak hal Guruku, terima kasihku";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}

Future<String> line9() async {
  String greeting = "Terima Kasih Telah Bernyanyi Bersama";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}